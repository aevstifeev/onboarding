### Overview
The features provided by the Secure Team are mostly leveraging tools that are executed during pipelines,
using Docker images.
That’s why it’s crucial to set up a development environment with the [GitLab CI Runner](https://gitlab.com/gitlab-org/gitlab-runner).

This document will guide you through the whole process. Don't hesitate to ask questions in Slack if anything is unclear:

- `#s_secure` for things related to Secure
- `#questions` for any general questions
- `#gdk` for [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit) related questions

Enjoy!

Need some help? Ping one of your Secure team mates, I'm sure they will be more than willing to answer your call! You can find out who they are and where they at the [Better Team Page](https://leipert-projects.gitlab.io/better-team-page/?search=departments%3ASecure%20Section).

#### General
1. [ ] Read about GitLab Secure products and how they are set up and work. [docs](https://docs.gitlab.com/ee/user/application_security/)
1. [ ] All repositories maintained by the Secure team is present [here](https://gitlab.com/gitlab-org/security-products), you can bookmark it.
1. [ ] The [Secure Group](https://gitlab.com/gitlab-org/secure) is another group which is used to address members of the Secure team and to keep other projects which do not directly affect the product. For instance the [onboarding](https://gitlab.com/gitlab-org/secure/onboarding).

### Day 6: Setup

#### Accounts
1. [ ] Manager: Add the new member to [https://staging.gitlab.com/groups/secure-team-test](https://staging.gitlab.com/groups/secure-team-test) as Maintainer
1. [ ] Manager: Add the new member to [https://gitlab.com/groups/gitlab-org/security-products/-/group_members](https://gitlab.com/groups/gitlab-org/security-products/-/group_members) as Maintainer
1. [ ] Manager: Add the new member to [https://gitlab.com/groups/gitlab-org/secure/-/group_members](https://gitlab.com/groups/gitlab-org/secure/-/group_members) as Maintainer
1. [ ] Manager: Add the new member to the corresponding team's subgroup as Maintainer:
    - Composition Analysis: https://gitlab.com/gitlab-org/secure/composition-analysis-be
    - Dynamic Analysis: https://gitlab.com/gitlab-org/secure/dynamic-analysis-be
    - Static Analysis: https://gitlab.com/gitlab-org/secure/static-analysis-be
    - Secure Frontend: https://gitlab.com/gitlab-org/secure/frontend
1. [ ] Manager: Add the new member to [Geekbot](https://geekbot.io/)
1. [ ] Manager: Add the member to the appropriate slack groups, `@secure_dynamic_analysis_be`, `@secure_composition_analysis_be`, `@secure_static_analysis_be` or `@secure-fe` by opening an Access Request
1. [ ] Manager: Add the new member to the [Secure Stage Calendar](https://calendar.google.com/calendar/r/settings/calendar/Z2l0bGFiLmNvbV9tZDBhbzM2Z3B2bDV2MWY0MTI4ZXJobmo2Z0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) and explain what it is.
1. [ ] New team member: Generate a [license](https://about.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee) or ask your manager to do so if you don't have access to dev.gitlab.org
1. [ ] New team member: Create a [new access request](https://gitlab.com/gitlab-com/access-requests/issues/new) for Periscope with `Editor` access level ([example](https://gitlab.com/gitlab-com/access-requests/issues/1285))


#### GDK
1. [ ] Install all needed [dependencies for GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/prepare.md). Watch [video on GDK installation](https://www.youtube.com/watch?v=gxn-0KSfNaU)
1. [ ] Secure Team's primary target is the GitLab Enterprise Edition. By default, GDK installs the Community Edition,
       so [follow these instructions](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/set-up-gdk.md#gitlab-enterprise-edition)
       to install the Enterprise Edition with GDK.
1. [ ] Upload your Ultimate license key for GitLab Enterprise Edition by visiting `http://<your-gitlab-ee-url>:3001/admin/license/new`
1. [ ] You may need Community Edition for some contribution (if you alter database schema).
       As well, it could be useful to set up two GDKs side-by-side, one for CE and one for EE,
       as you might need to work on both for the same feature.
1. [ ] If you’re stuck, ask for help in `#development`, `#gdk` Slack channels. Use Slack search for your questions
       first with filter `in:#gdk`. There is a possibility that someone has already had a similar issue.
1. [ ] Alternatively, you can install [gitlab-compose-kit](https://gitlab.com/gitlab-org/gitlab-compose-kit)
       if you’re familiar with Docker.
       It makes it easy to switch back and forth between GitLab CE and GitLab EE.
1. [ ] Check [How to use GitLab Development Kit doc](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/README.md)
1. [ ] You’ll need to know [commands](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/HELP)
       to operate your local environment successfully.
       For example, to start database locally (to run tests), you need to start db with `gdk run db`.
1. [ ] GDK contains a collection of resources that help running an instance of GitLab locally as well as
       GitLab codebase itself. Code of EE or CE version can be found in `/gitlab` folder of GDK.
       Check this folder.
1. [ ] If you have not already, add yourself as a reviewer for GitLab CE and EE by adding a [`projects` key](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/team.yml#L16) to your entry in gitlab.com's `data/team.yml`. This will allow you to be automatically suggested as a reviewer for MRs.

#### Runner
1. [ ] Consider reading and watching this workshop ([Description](https://docs.google.com/document/d/1HnT2CCtaE7MMD7Qkd8_MLugwICDqu9Lp3f48o76Dzwk),
                                                           [Video](https://drive.google.com/file/d/17JG1Tp6no6gea4y5CRbsXSNzNNHAlcFK/view))
       about setup GitLab CI. It contains valuable info about
       Docker with Docker Machine and custom domain name.
1. [ ] Install GitLab Runner locally with [this tutorial](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/runner.md).
       Be sure to follow the Docker related setup instructions. (Optionally: you can setup your Runners to run inside containers by following these [instructions](https://docs.gitlab.com/runner/install/docker.html#docker-image-installation-and-configuration))
1. [ ] Register your runner with the command: `gitlab-runner register -c <gdk-path>/gitlab-runner-config.toml`,
       choose `Docker` as an executor. This command will generate the `gitlab-runner-config.toml` file.
1. [ ] Run the GitLab Runner in the foreground with the command:
`gitlab-runner --log-level debug run --config <gdk-path>/gitlab-runner-config.toml`
1. [ ] If you have questions about the runner or you're stuck and need help, ask in `#ci_runner` Slack channel.
1. [ ] Make your runner **[privileged](https://docs.gitlab.com/runner/executors/docker.html#use-docker-in-docker-with-privileged-mode)** (`<gdk-path>/gitlab-runner-config.toml`: `privileged = true`). You need this setting to run
       "Docker in Docker". It's a requirement for secure jobs as the [SAST analyzers](https://gitlab.com/gitlab-org/security-products/analyzers) run as docker containers, which are spawned by the [orchestrator](https://gitlab.com/gitlab-org/security-products/analyzers/common/blob/master/orchestrator/orchestrator.go#L49). Since the orchestrator itself is running inside a container, we need to use the docker-in-docker paradigm.
       [Read more about it](https://docs.gitlab.com/runner/executors/docker.html#use-docker-in-docker-with-privileged-mode)

### Day 7: Secure playground
At this step you should have on your local machine:
 * installed gdk and running a local instance of GitLab EE (gdk run),
 * running and connected to GitLab CI Runner.

#### Security Reports Overview
Importing and running CI pipelines on this sample project will populate your security report dashboard.

1. [ ] On your local instance of GitLab, import by URL [Security Report Examples](https://gitlab.com/gitlab-examples/security/security-reports).
1. [ ] Add this project to a [group](https://docs.gitlab.com/ee/user/group/#add-projects-to-a-group).
1. [ ] Kick off the CI process (pipeline) by following the [project README](https://gitlab.com/gitlab-examples/security/security-reports/blob/master/README.md).
1. [ ] Check the `Security` tab of the pipeline results, there should be a list of vulnerabilities.
1. [ ] Inspect the `.gitlab-ci.yml` file and understand how the project is setup to generate reports. You can find more about the project while setting up Runners and running them locally.


#### SAST
1. [ ] On your local instance of GitLab, import by URL [test SAST project](https://gitlab.com/gitlab-org/security-products/tests/sast).
1. [ ] Add this project to a group. It will allow you to enable security dashboard correctly.
1. [ ] Create and run a pipeline for `master` branch of the previously uploaded project.
1. [ ] Learn more about [SAST environment variables](https://gitlab.com/gitlab-org/security-products/sast#settings).
1. [ ] At this step, you may need tuning the runner with environment variables listed in
the [GitLab SAST tool Readme](https://gitlab.com/gitlab-org/security-products/sast/blob/master/README.md).
For example, if you have an unstable internet connection, you may need to set or increase `SAST_PULL_ANALYZER_IMAGE_TIMEOUT`. One way this can easily be done is through your [pipeline's variables](https://docs.gitlab.com/ee/ci/variables/#variables).
1. [ ] Now you should have a green pipeline. On success, the pipeline should show
       the security report having uploaded as an artifact.
       This report will be used to populate the security dashboards.
1. [ ] Go to the pipeline page and explore the “Security” tab in the pipeline.
       When clicking on a vulnerability, GitLab shows a modal window with more information (like the file
       where the vulnerability has been found) and actions (dismiss and create an issue).
       Note: there must be an existing security report artifact generated by the
       pipeline in order for that tab to exist. Artifacts are generally deleted
       after some time, so old pipelines won't have a Security tab.

#### DAST
1. [ ] Execute a pipeline on the `develop` branch in the [webgoat](https://gitlab.com/gitlab-org/security-products/tests/webgoat) project and inspect the Security reports and dashboards.
1. [ ] On your local instance of GitLab, import by URL [webgoat](https://gitlab.com/gitlab-org/security-products/tests/webgoat) and execute pipeline in the `develop` branch.
1. [ ] Clone the [dast](https://gitlab.com/gitlab-org/security-products/dast) project on your system.
1. [ ] Run the [tests](https://gitlab.com/gitlab-org/security-products/dast#tests) present in the project.
1. [ ] Run [railsgoat](https://github.com/OWASP/railsgoat) (follow the instructions on the README for running inside Docker) and run a DAST scan against it using the instructions present [here](https://gitlab.com/gitlab-org/security-products/dast#how-to-use) (in the `docker run` command point it to your instance of railsgoat)
1. [ ] Inspect the captured vulnerabilities: `jq .site[0].alerts gl-dast-report.json`
1. [ ] Import the railsgoat project into your GitLab instance and add the `.gitlab-ci.yml` for running DAST. Refer the [documentation](https://docs.gitlab.com/ee/user/application_security/dast) for doing that.

#### Composition Analysis
1. [ ] [Feature Overview video summary](https://www.youtube.com/watch?v=mZvehoa4JdU&list=PL05JrBw4t0Kq7yUrZazEF3diazV29RRo1&index=7&t=0s). Contents include: Dependency Scanning, Container Scanning, License Management, Vulnerability DB, Analyzers, and data sources (DB and artifacts).

#### AutoDevOps
GitLab can automatically configure the entire CI/CD pipeline for projects (without the need to create the `.gitlab-ci.yml` file), this feature is known as [AutoDevOps](https://docs.gitlab.com/ee/topics/autodevops/index.html)
As part of this task you need to:
1. [ ] Create a new project on GitLab.com and create Kubernetes cluster for your project by following this [guide](https://gitlab.com/help/topics/autodevops/quick_start_guide.md#creating-a-new-project-from-a-template), your GitLab google account should already have access to the `gitlab-demo` and `gitlab-internal` [GCP](https://cloud.google.com) projects; create a cluster in the `gitlab-internal` project.
1. [ ] Install the required Kubernetes plugins as mentioned in the documentation linked above.
1. [ ] In your project go to ` Settings > CI/CD > Auto DevOps` and enable it. This should automatically spawn a new pipeline.
1. [ ] Make sure your pipeline (`CI/CD > Pipelines`) has an `Auto DevOps` label on it, it has all the Secure section jobs in it and that it eventually passes.


#### Prepare to contribute

1. [ ] Configure GPG to [sign your commits](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/).
1. [ ] Create an MR with improvements to this [document](https://gitlab.com/gitlab-org/secure/onboarding/blob/master/.gitlab/issue_templates/Technical_Onboarding.md).
1. [ ] You are unlikely to work on a security fix as part of your first Deliverables, but once you are assigned to one please refer to the [security issue workflow](https://about.gitlab.com/handbook/engineering/workflow/#security-issues) and remember it is okay to ask questions.

### Now you are ready for new tasks
You're awesome!

## Troubleshooting

### Security Dashboard won't show vulnerabilities

TLDR: Re-run the pipeline on the default branch

There is currently a distinction between the data that populates the project
security dashboard (PSD) and group security dashboard (GSD).

The PSD pulls vulnerabilities directly from the last pipeline on the default branch
(usually `master`). This requires the last pipeline to have been ran and to have
uploaded a security report.

The GSD pulls vulnerabilities directly from the database, as `Vulnerabilities::Occurrence`
records. This requires the last pipeline to have been ran on the default branch
and been processed through our CI parsers.
