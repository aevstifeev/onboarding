# Onboarding - Secure

This project helps the Secure team to onboard its new members.
An [issue template](.gitlab/issue_templates/Technical_Onboarding.md) is used to 
create onboarding issues, which follows the general onboarding ones. 
That's why they start at day 6.